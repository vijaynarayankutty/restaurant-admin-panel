#require Rails.root.join('lib','rails_admin','order.rb')
require Rails.root.join('lib', 'open_order.rb')
require Rails.root.join('lib', 'closed_order.rb')
require Rails.root.join('lib', 'history_order.rb')
require Rails.root.join('lib', 'confirmed_order.rb')
require Rails.root.join('lib', 'list_dish.rb')
require Rails.root.join('lib', 'detail_order.rb')
require Rails.root.join('lib', 'add_dish.rb')
# RailsAdmin::Config::Actions.register(RailsAdmin::Config::Actions::OpenOrder)

RailsAdmin.config do |config|

  ### Popular gems integration
  # config.authenticate_with do
  #   warden.authenticate! scope: :admin
  # end
  # config.current_user_method(&:current_admin)
 #  config.model 'Restaurant' do 
 #    edit do 
 #      field :beacon_id
 #    end
 #  end
   
 #   config.model 'TableManagement' do
 #    edit do
 #      field :uuid
 #   end
 # end

  config.main_app_name = ["Restaurant"]
  config.authorize_with :cancan
  config.current_user_method &:current_admin
  ## == Devise ==
  # config.authenticate_with do
  #   warden.authenticate! scope: :user
  # end
  # config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete

   
    detail_order do
     visible do
       bindings[:abstract_model].model.to_s == "Order"
      end
    end
    
    open_order do
      # Make it visible only for article model. You can remove this if you don't need.
      visible do
        bindings[:abstract_model].model.to_s == "Restaurant"
      end
    end

    closed_order do
      visible do
        bindings[:abstract_model].model.to_s == "Restaurant"
      end
    end

    history_order do 
      visible do
        bindings[:abstract_model].model.to_s == "Restaurant"
      end
    end

    list_dish do
      visible do
        bindings[:abstract_model].model.to_s == "Restaurant"
      end
    end

    add_dish do
      visible do
        bindings[:abstract_model].model.to_s == "Restaurant"
      end
    end
    # confirmed_order do 
    #   visible do
    #     bindings[:abstract_model].model.to_s == "Restaurant"
    #   end
    # end

    # history_order do
    #   visible do
    #     bindings["abstract_model"].model.to_s == "Restaurant"
    #   end
    # end

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end

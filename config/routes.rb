require 'api_constraints'
Rails.application.routes.draw do
  resources :orders

  devise_for :restaurant_admins
  resources :restaurants

  devise_for :users


  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :users, :only => [:show, :create, :update, :destroy]
      resources :sessions, :only => [:create, :destroy]
      resources :dishes
      resources :restaurants
      resources :dish_categories
      resources :restaurant_categories
      resources :orders, :only => [:create, :update, :destroy]
    end
    # scope module: :v2, constraints: ApiConstraints.new(version: 2) do
    #   # resources :products
    # end
  end

  get 'index/home'

  #root 'static_pages#page_404'
  
  resources :dishes

  resources :dish_categories

  resources :restaurant_categories

  devise_for :admin
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  
   get '/static_pages/page_404' => 'static_pages#page_404'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  devise_scope :admin do
    get "admin/sign_in", to: "devise/sessions#new"
    root 'devise/sessions#new'
  end

  #root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

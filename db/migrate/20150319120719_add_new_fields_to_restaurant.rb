class AddNewFieldsToRestaurant < ActiveRecord::Migration
  def change
  	add_column :restaurants, :todays_status, :string
  	add_column :restaurants, :dining_cost_rating, :integer
  end
end

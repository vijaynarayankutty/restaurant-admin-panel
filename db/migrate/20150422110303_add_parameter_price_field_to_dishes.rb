class AddParameterPriceFieldToDishes < ActiveRecord::Migration
  def change
  	add_column  :dishes, :parameter_1_price_4, :decimal
  	add_column  :dishes, :parameter_1_price_5, :decimal
  	add_column  :dishes, :parameter_2_price_4, :decimal
  	add_column  :dishes, :parameter_2_price_5, :decimal
  	add_column  :dishes, :parameter_3_price_4, :decimal
  	add_column  :dishes, :parameter_3_price_5, :decimal
  	add_column  :dishes, :parameter_4_price_4, :decimal
  	add_column  :dishes, :parameter_4_price_5, :decimal
  	add_column  :dishes, :parameter_5_price_4, :decimal
  	add_column  :dishes, :parameter_5_price_5, :decimal
  end
end

class AddTimeFieldTorestaurants < ActiveRecord::Migration
  def change
  	add_column :restaurants, :from_time, :time 
  	add_column :restaurants, :to_time, :time
  	add_column :restaurants, :rating, :decimal
  end
end

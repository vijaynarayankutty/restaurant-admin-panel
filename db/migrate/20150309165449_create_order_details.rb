class CreateOrderDetails < ActiveRecord::Migration
  def change
    create_table :order_details do |t|
      t.references :dish, index: true
      t.references :order, index: true

      t.timestamps null: false
    end
    add_foreign_key :order_details, :dishes
    add_foreign_key :order_details, :orders
  end
end

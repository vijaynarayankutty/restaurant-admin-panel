class AddFieldToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :merchant_description, :text
  	add_column :orders, :mail_receipt, :integer
  end
end

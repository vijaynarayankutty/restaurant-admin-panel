class AddAndRemoveColumFromOrders < ActiveRecord::Migration
  def change
  	remove_column :orders, :table_no
  	add_column :orders, :table_no, :string
  	add_column :orders, :customer_instruction, :string
  	add_column :orders, :order_status, :string
  end
end

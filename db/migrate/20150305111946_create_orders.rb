class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.decimal :amount
      t.decimal :tax
      t.decimal :tip
      t.integer :transaction_id
      t.integer :table_no
      t.datetime :time_to_dispatch_order
      t.boolean :dine_in
      t.references :restaurant, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :orders, :restaurants
    add_foreign_key :orders, :users
  end
end

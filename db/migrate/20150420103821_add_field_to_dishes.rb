class AddFieldToDishes < ActiveRecord::Migration
  def change
  	remove_column :dishes, :price
  	remove_column :dishes, :dish_propert_1
  	remove_column :dishes, :dish_propert_2
  	remove_column :dishes, :dish_propert_3
  	remove_column :dishes, :parameter_1_price_1
  	remove_column :dishes, :parameter_1_price_2
  	remove_column :dishes, :parameter_1_price_3
  	remove_column :dishes, :parameter_2_price_1
  	remove_column :dishes, :parameter_2_price_2
  	remove_column :dishes, :parameter_2_price_3
  	remove_column :dishes, :parameter_3_price_1
  	remove_column :dishes, :parameter_3_price_2
  	remove_column :dishes, :parameter_3_price_3
  	remove_column :dishes, :parameter_4_price_1
  	remove_column :dishes, :parameter_4_price_2
  	remove_column :dishes, :parameter_4_price_3
  	remove_column :dishes, :parameter_5_price_1
  	remove_column :dishes, :parameter_5_price_2
  	remove_column :dishes, :parameter_5_price_3
  	


  	add_column :dishes, :parameter_1_price_1, :decimal
  	add_column :dishes, :parameter_1_price_2, :decimal
  	add_column :dishes, :parameter_1_price_3, :decimal
  	add_column :dishes, :parameter_2_price_1, :decimal
  	add_column :dishes, :parameter_2_price_2, :decimal
  	add_column :dishes, :parameter_2_price_3, :decimal
  	add_column :dishes, :parameter_3_price_1, :decimal
  	add_column :dishes, :parameter_3_price_2, :decimal
  	add_column :dishes, :parameter_3_price_3, :decimal
  	add_column :dishes, :parameter_4_price_1, :decimal
  	add_column :dishes, :parameter_4_price_2, :decimal
  	add_column :dishes, :parameter_4_price_3, :decimal
  	add_column :dishes, :parameter_5_price_1, :decimal
  	add_column :dishes, :parameter_5_price_2, :decimal
  	add_column :dishes, :parameter_5_price_3, :decimal
  	add_column :dishes, :dish_property_chilly, :integer
  	add_column :dishes, :dish_property_vegan, :integer
  	add_column :dishes, :dish_property_glutenFree, :integer
  	add_column :dishes, :dish_property_vegetarian, :integer
  	add_column :dishes, :dish_property_nutFree, :integer


  end
end

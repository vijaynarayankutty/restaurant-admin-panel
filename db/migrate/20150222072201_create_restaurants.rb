class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :restaurant_name
      t.text :description
      t.datetime :available_from
      t.datetime :available_to
      t.integer :no_of_tables
      t.string :email
      t.text :address1
      t.text :address2
      t.string :country
      t.string :location
      t.string :city
      t.string :phonenumber
      t.string :latitude
      t.string :longitude
      # t.references :admin, index: true
      t.references :restaurant_category, index: true
      t.boolean :active
      t.string  :auth_token, default: "", unique: true
      t.timestamps null: false
    end
     # add_foreign_key :restaurants, :admins
    add_foreign_key :restaurants, :restaurant_categories
  end
end

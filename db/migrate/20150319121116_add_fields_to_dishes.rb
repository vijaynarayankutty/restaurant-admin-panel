class AddFieldsToDishes < ActiveRecord::Migration
  def change
  	add_column :dishes, :parameter_4, :string
  	add_column :dishes, :parameter_5, :string

   	add_column :dishes, :parameter1_options, :string
   	add_column :dishes, :parameter2_options, :string
   	add_column :dishes, :parameter3_options, :string

   	add_column :dishes, :parameter_1_price_1, :integer
   	add_column :dishes, :parameter_1_price_2, :integer
   	add_column :dishes, :parameter_1_price_3, :integer
   	add_column :dishes, :parameter_2_price_1, :integer
   	add_column :dishes, :parameter_2_price_2, :integer
   	add_column :dishes, :parameter_2_price_3, :integer
   	add_column :dishes, :parameter_3_price_1, :integer
   	add_column :dishes, :parameter_3_price_2, :integer
   	add_column :dishes, :parameter_3_price_3, :integer
   	add_column :dishes, :parameter_4_price_1, :integer
   	add_column :dishes, :parameter_4_price_2, :integer
   	add_column :dishes, :parameter_4_price_3, :integer
   	add_column :dishes, :parameter_5_price_1, :integer
   	add_column :dishes, :parameter_5_price_2, :integer
   	add_column :dishes, :parameter_5_price_3, :integer

   	add_column :dishes, :dish_propert_1, :integer
   	add_column :dishes, :dish_propert_2, :integer
   	add_column :dishes, :dish_propert_3, :integer
  end
end

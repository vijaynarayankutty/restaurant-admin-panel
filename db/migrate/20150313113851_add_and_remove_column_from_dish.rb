class AddAndRemoveColumnFromDish < ActiveRecord::Migration
  def change
  	remove_column :dishes, :price
  	add_column    :dishes, :price, :decimal
  end
end

class AddColumnToDishes < ActiveRecord::Migration
  def change
  	add_column :dishes, :discount, :decimal
  	add_column :dishes, :updated_user, :string
  end
end

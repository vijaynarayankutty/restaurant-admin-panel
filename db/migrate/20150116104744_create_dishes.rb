class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.text :description
      t.string :price
      t.string :availabletime
      t.references :dish_category, index: true
      t.boolean :active
      t.references :restaurant, index: true

      t.timestamps null: false
    end
  end
end

class CreateTableManagements < ActiveRecord::Migration
  def change
    create_table :table_managements do |t|
      t.string :table_name
      t.references :restaurant, index: true
      t.string :uuid
      t.integer :major
      t.integer :minor
      t.text :description
      t.string :entry_message
      t.string :exit_message

      t.timestamps null: false
    end
    add_foreign_key :table_managements, :restaurants
  end
end

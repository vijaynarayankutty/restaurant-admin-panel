class AddAdminidTorestaurants < ActiveRecord::Migration
  def change
  	add_column :restaurants, :restaurant_admin_id, :integer, index: true
  end
end

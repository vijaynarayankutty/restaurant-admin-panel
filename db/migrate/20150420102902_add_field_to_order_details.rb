class AddFieldToOrderDetails < ActiveRecord::Migration
  def change
  	add_column :order_details, :parameter_4, :string
  	add_column :order_details, :parameter_5, :string
  	add_column :order_details, :option_4, :string
  	add_column :order_details, :option_5, :string
  	add_column :order_details, :order_dish_status, :string
  end
end

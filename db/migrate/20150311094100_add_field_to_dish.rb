class AddFieldToDish < ActiveRecord::Migration
  def change
  	add_column :dishes, :parameter_1, :string
  	add_column :dishes, :parameter_2, :string
  	add_column :dishes, :parameter_3, :string
  	add_column :dishes, :option_1, :string
  	add_column :dishes, :option_2, :string
  	add_column :dishes, :option_3, :string
  end
end

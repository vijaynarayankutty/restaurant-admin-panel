class AddFieldAverageCookingTimeToDishes < ActiveRecord::Migration
  def change
  	add_column :dishes, :average_cooking_time, :integer
  end
end

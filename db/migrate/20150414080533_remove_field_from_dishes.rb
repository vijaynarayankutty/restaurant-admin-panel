class RemoveFieldFromDishes < ActiveRecord::Migration
  def change
  	remove_column :dishes, :option_1
  	remove_column :dishes, :option_2
  	remove_column :dishes, :option_3
  end
end

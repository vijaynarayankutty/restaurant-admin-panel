class AddFieldTaxTorestaurant < ActiveRecord::Migration
  def change
  	add_column :restaurants, :tax, :decimal
  end
end

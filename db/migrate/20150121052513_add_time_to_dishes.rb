class AddTimeToDishes < ActiveRecord::Migration
  def change
  	add_column :dishes, :availablefrom, :time 
  	add_column :dishes, :availableto, :time 
  end
end

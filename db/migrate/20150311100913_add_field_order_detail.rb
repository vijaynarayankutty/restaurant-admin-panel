class AddFieldOrderDetail < ActiveRecord::Migration
  def change
  	add_column :order_details, :parameter_1, :string
  	add_column :order_details, :parameter_2, :string
  	add_column :order_details, :parameter_3, :string
  	add_column :order_details, :option_1, :string
  	add_column :order_details, :option_2, :string
  	add_column :order_details, :option_3, :string
  	add_column :order_details, :quantity, :float , :limit => 53
  end
end

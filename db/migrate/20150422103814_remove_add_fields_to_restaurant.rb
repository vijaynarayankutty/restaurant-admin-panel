class RemoveAddFieldsToRestaurant < ActiveRecord::Migration
  def change
  	remove_column :restaurants, :address1
  	remove_column :restaurants, :address2
  	remove_column :restaurants, :location
  	add_column :restaurants, :short_Address, :text
  	add_column :restaurants, :long_Address,  :text
  end
end

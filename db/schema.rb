# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150429091157) do

  create_table "admins", force: true do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "address",                limit: 255
    t.string   "phone",                  limit: 255
    t.string   "city",                   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                   limit: 255
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "dish_categories", force: true do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "dishes", force: true do |t|
    t.string   "name",                     limit: 255
    t.text     "description",              limit: 65535
    t.integer  "dish_category_id",         limit: 4
    t.boolean  "active",                   limit: 1
    t.integer  "restaurant_id",            limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.time     "availablefrom"
    t.time     "availableto"
    t.string   "image_file_name",          limit: 255
    t.string   "image_content_type",       limit: 255
    t.integer  "image_file_size",          limit: 4
    t.datetime "image_updated_at"
    t.string   "parameter_1",              limit: 255
    t.string   "parameter_2",              limit: 255
    t.string   "parameter_3",              limit: 255
    t.string   "parameter_4",              limit: 255
    t.string   "parameter_5",              limit: 255
    t.string   "parameter1_options",       limit: 255
    t.string   "parameter2_options",       limit: 255
    t.string   "parameter3_options",       limit: 255
    t.decimal  "discount",                               precision: 10
    t.string   "updated_user",             limit: 255
    t.string   "parameter4_options",       limit: 255
    t.string   "parameter5_options",       limit: 255
    t.decimal  "parameter_1_price_1",                    precision: 10
    t.decimal  "parameter_1_price_2",                    precision: 10
    t.decimal  "parameter_1_price_3",                    precision: 10
    t.decimal  "parameter_2_price_1",                    precision: 10
    t.decimal  "parameter_2_price_2",                    precision: 10
    t.decimal  "parameter_2_price_3",                    precision: 10
    t.decimal  "parameter_3_price_1",                    precision: 10
    t.decimal  "parameter_3_price_2",                    precision: 10
    t.decimal  "parameter_3_price_3",                    precision: 10
    t.decimal  "parameter_4_price_1",                    precision: 10
    t.decimal  "parameter_4_price_2",                    precision: 10
    t.decimal  "parameter_4_price_3",                    precision: 10
    t.decimal  "parameter_5_price_1",                    precision: 10
    t.decimal  "parameter_5_price_2",                    precision: 10
    t.decimal  "parameter_5_price_3",                    precision: 10
    t.integer  "dish_property_chilly",     limit: 4
    t.integer  "dish_property_vegan",      limit: 4
    t.integer  "dish_property_glutenFree", limit: 4
    t.integer  "dish_property_vegetarian", limit: 4
    t.integer  "dish_property_nutFree",    limit: 4
    t.decimal  "price",                                  precision: 10
    t.decimal  "parameter_1_price_4",                    precision: 10
    t.decimal  "parameter_1_price_5",                    precision: 10
    t.decimal  "parameter_2_price_4",                    precision: 10
    t.decimal  "parameter_2_price_5",                    precision: 10
    t.decimal  "parameter_3_price_4",                    precision: 10
    t.decimal  "parameter_3_price_5",                    precision: 10
    t.decimal  "parameter_4_price_4",                    precision: 10
    t.decimal  "parameter_4_price_5",                    precision: 10
    t.decimal  "parameter_5_price_4",                    precision: 10
    t.decimal  "parameter_5_price_5",                    precision: 10
    t.integer  "average_cooking_time",     limit: 4
    t.integer  "todays_status",            limit: 4
    t.integer  "dish_property_seafood",    limit: 4
  end

  add_index "dishes", ["dish_category_id"], name: "index_dishes_on_dish_category_id", using: :btree
  add_index "dishes", ["restaurant_id"], name: "index_dishes_on_restaurant_id", using: :btree

  create_table "order_details", force: true do |t|
    t.integer  "dish_id",           limit: 4
    t.integer  "order_id",          limit: 4
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "parameter_1",       limit: 255
    t.string   "parameter_2",       limit: 255
    t.string   "parameter_3",       limit: 255
    t.string   "option_1",          limit: 255
    t.string   "option_2",          limit: 255
    t.string   "option_3",          limit: 255
    t.float    "quantity",          limit: 53
    t.string   "parameter_4",       limit: 255
    t.string   "parameter_5",       limit: 255
    t.string   "option_4",          limit: 255
    t.string   "option_5",          limit: 255
    t.string   "order_dish_status", limit: 255
  end

  add_index "order_details", ["dish_id"], name: "index_order_details_on_dish_id", using: :btree
  add_index "order_details", ["order_id"], name: "index_order_details_on_order_id", using: :btree

  create_table "orders", force: true do |t|
    t.decimal  "amount",                               precision: 10
    t.decimal  "tax",                                  precision: 10
    t.decimal  "tip",                                  precision: 10
    t.datetime "time_to_dispatch_order"
    t.boolean  "dine_in",                limit: 1
    t.integer  "restaurant_id",          limit: 4
    t.integer  "user_id",                limit: 4
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.boolean  "active",                 limit: 1
    t.string   "table_no",               limit: 255
    t.string   "customer_instruction",   limit: 255
    t.string   "order_status",           limit: 255
    t.text     "merchant_description",   limit: 65535
    t.integer  "mail_receipt",           limit: 4
  end

  add_index "orders", ["restaurant_id"], name: "index_orders_on_restaurant_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "restaurant_admins", force: true do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "role",                   limit: 255
    t.string   "address",                limit: 255
    t.string   "phone",                  limit: 255
    t.string   "city",                   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "restaurant_admins", ["email"], name: "index_restaurant_admins_on_email", unique: true, using: :btree
  add_index "restaurant_admins", ["reset_password_token"], name: "index_restaurant_admins_on_reset_password_token", unique: true, using: :btree

  create_table "restaurant_categories", force: true do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "restaurants", force: true do |t|
    t.string   "restaurant_name",              limit: 255
    t.text     "description",                  limit: 65535
    t.datetime "available_from"
    t.datetime "available_to"
    t.integer  "no_of_tables",                 limit: 4
    t.string   "email",                        limit: 255
    t.string   "country",                      limit: 255
    t.string   "city",                         limit: 255
    t.string   "phonenumber",                  limit: 255
    t.string   "latitude",                     limit: 255
    t.string   "longitude",                    limit: 255
    t.integer  "restaurant_category_id",       limit: 4
    t.boolean  "active",                       limit: 1
    t.string   "auth_token",                   limit: 255,                  default: ""
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "image_file_name",              limit: 255
    t.string   "image_content_type",           limit: 255
    t.integer  "image_file_size",              limit: 4
    t.datetime "image_updated_at"
    t.integer  "user_id",                      limit: 4
    t.time     "from_time"
    t.time     "to_time"
    t.decimal  "rating",                                     precision: 10
    t.integer  "restaurant_admin_id",          limit: 4
    t.string   "countrycode",                  limit: 255
    t.decimal  "tax",                                        precision: 10
    t.string   "todays_status",                limit: 255
    t.integer  "dining_cost_rating",           limit: 4
    t.string   "restaurant_logo_file_name",    limit: 255
    t.string   "restaurant_logo_content_type", limit: 255
    t.integer  "restaurant_logo_file_size",    limit: 4
    t.datetime "restaurant_logo_updated_at"
    t.text     "short_Address",                limit: 65535
    t.text     "long_Address",                 limit: 65535
  end

  add_index "restaurants", ["restaurant_category_id"], name: "index_restaurants_on_restaurant_category_id", using: :btree

  create_table "table_managements", force: true do |t|
    t.string   "table_name",    limit: 255
    t.integer  "restaurant_id", limit: 4
    t.string   "uuid",          limit: 255
    t.integer  "major",         limit: 4
    t.integer  "minor",         limit: 4
    t.text     "description",   limit: 65535
    t.string   "entry_message", limit: 255
    t.string   "exit_message",  limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "table_managements", ["restaurant_id"], name: "index_table_managements_on_restaurant_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  limit: 255,   default: "", null: false
    t.string   "encrypted_password",     limit: 255,   default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.text     "address_one",            limit: 65535
    t.text     "address_two",            limit: 65535
    t.string   "country",                limit: 255
    t.string   "countrycode",            limit: 255
    t.string   "city",                   limit: 255
    t.integer  "zipcode",                limit: 4
    t.string   "mobile",                 limit: 255
    t.string   "CreditCard",             limit: 255
    t.string   "CVV",                    limit: 255
    t.integer  "refferal_total",         limit: 4
    t.string   "provider_name",          limit: 255
    t.string   "auth_token",             limit: 255,   default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "order_details", "dishes"
  add_foreign_key "order_details", "orders"
  add_foreign_key "orders", "restaurants"
  add_foreign_key "orders", "users"
  add_foreign_key "restaurants", "restaurant_categories"
  add_foreign_key "table_managements", "restaurants"
end

require 'test_helper'

class RestaurantsControllerTest < ActionController::TestCase
  setup do
    @restaurant = restaurants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:restaurants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create restaurant" do
    assert_difference('Restaurant.count') do
      post :create, restaurant: { active: @restaurant.active, address1: @restaurant.address1, address2: @restaurant.address2, admin_id: @restaurant.admin_id, available_from: @restaurant.available_from, available_to: @restaurant.available_to, city: @restaurant.city, country: @restaurant.country, countrycode: @restaurant.countrycode, description: @restaurant.description, email: @restaurant.email, latitude: @restaurant.latitude, location: @restaurant.location, longitude: @restaurant.longitude, no_of_tables: @restaurant.no_of_tables, phonenumber: @restaurant.phonenumber, restaurant_category_id: @restaurant.restaurant_category_id, restaurant_name: @restaurant.restaurant_name }
    end

    assert_redirected_to restaurant_path(assigns(:restaurant))
  end

  test "should show restaurant" do
    get :show, id: @restaurant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @restaurant
    assert_response :success
  end

  test "should update restaurant" do
    patch :update, id: @restaurant, restaurant: { active: @restaurant.active, address1: @restaurant.address1, address2: @restaurant.address2, admin_id: @restaurant.admin_id, available_from: @restaurant.available_from, available_to: @restaurant.available_to, city: @restaurant.city, country: @restaurant.country, countrycode: @restaurant.countrycode, description: @restaurant.description, email: @restaurant.email, latitude: @restaurant.latitude, location: @restaurant.location, longitude: @restaurant.longitude, no_of_tables: @restaurant.no_of_tables, phonenumber: @restaurant.phonenumber, restaurant_category_id: @restaurant.restaurant_category_id, restaurant_name: @restaurant.restaurant_name }
    assert_redirected_to restaurant_path(assigns(:restaurant))
  end

  test "should destroy restaurant" do
    assert_difference('Restaurant.count', -1) do
      delete :destroy, id: @restaurant
    end

    assert_redirected_to restaurants_path
  end
end

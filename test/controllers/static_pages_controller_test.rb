require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get page_404" do
    get :page_404
    assert_response :success
  end

end

require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'
 
module RailsAdmin
  module Config
    module Actions
      class ListDish < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)

        register_instance_option :member do
          true
        end

        register_instance_option :route_fragment do
          "list_dish_path"
        end

        
        # register_instance_option :visible? do
        #   authorized? && (bindings[:controller].main_app.url_for(bindings[:object]) rescue false)
        # end

        register_instance_option :http_methods do
          [:get, :post]
        end
     
        
        register_instance_option :controller do
          Proc.new do
            @dishes = @object.get_dishes
          end
        end

        register_instance_option :link_icon do
           'icon-signal'
        end
        
      end
    end
  end
end
require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'
 
module RailsAdmin
  module Config
    module Actions
      class AddDish < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)

        register_instance_option :member do
          true
        end

        register_instance_option :route_fragment do
          "add_dish_path"
        end

        
        # register_instance_option :visible? do
        #   authorized? && (bindings[:controller].main_app.url_for(bindings[:object]) rescue false)
        # end

        register_instance_option :http_methods do
          [:get, :post]
        end
     
        
        register_instance_option :controller do
          Proc.new do
            @restaurant = @object
            #@dish = @object.new
          end
        end

        register_instance_option :link_icon do
           'icon-signal'
        end
        
      end
    end
  end
end
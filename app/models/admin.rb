class Admin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

         before_save { self.email = email.downcase }
         validates :first_name, presence: true, length: {maximum: 10}
         validates :last_name, presence: true, length:  {maximum: 10}
         #Email validationvia REGEX
         VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
         validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false}
         validates :city, presence: true, length: {maximum: 20}
         validates :password_confirmation, presence: true, :length => { :minimum => 8, :maximum => 128 }
         #VALID_PHONE_REGEX = /\d{3}-\d{3}-\d{4}/
         #validates :phone, presence: true, format: { with: VALID_PHONE_REGEX }, :length => { :maximum => 11}, uniqueness: true
         validates :phone, presence: true,  uniqueness: true, numericality: true
         validates :role, presence: true





  def super_admin?
  	self.role == "admin"
  end
# 
  # def super_admin?
  #   self.email == "admin@mail.com"
  # end

  def name
  	self.first_name
  end

  #  def role_enum
  #   [[admin]]
  # end

 

	rails_admin do 
		list do
			# field :id
			field :first_name
			#field :last_name
			field :email
      field :phone
			field :last_sign_in_at
		end  
     edit do
       field :first_name
       field :last_name
       field :email
       field :password
       field :password_confirmation
       field :role
        field :role, :enum do
      enum do
        %w(admin)
      end
    end
        # field :addressone
        # field :addresstwo
       field :city
       field :phone

     end       	
	end
end

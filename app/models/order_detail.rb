class OrderDetail < ActiveRecord::Base
  belongs_to :dish
  belongs_to :order

  # def get_orders
  # 	self.order.name
  # end

  rails_admin do
		list do
			field :dish_id do 
				label "Dish id"
			end

			field :order_id do 
				label "Order id"
			end
			end
		end
end

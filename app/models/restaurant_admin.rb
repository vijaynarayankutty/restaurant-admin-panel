class RestaurantAdmin < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

         before_save { self.email = email.downcase }
         validates :first_name, presence: true, length: {maximum: 10}
         validates :last_name, presence: true, length:  {maximum: 10}

         #Email validationvia REGEX
         VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
         validates :email, presence: true, length: { maximum: 50 },
                    format: { with: VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false}

         validates :password_confirmation, presence: true, :length => { :minimum => 8, :maximum => 128 }
         #VALID_PHONE_REGEX = /\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}/
         validates :phone, presence: true,  uniqueness: true
         
         
         validates :address, presence: true, length: { maximum: 100 }
         validates :city, presence: true

 def name
 	self.first_name
 end

	rails_admin do
		list do
			field :first_name
			field :email
			field :phone
			field :last_sign_in_at
		end

		edit do
		   field :first_name
	       field :last_name
	       field :email
	       field :password
	       field :password_confirmation
	       field :address
	       field :phone
	       field :city
		end
	end
end
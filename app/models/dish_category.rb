class DishCategory < ActiveRecord::Base
	validates :name, presence: true, length: {maximum: 20}

	rails_admin do 
		list do
			field :name
			field :created_at
			field :updated_at
		end
	end
end

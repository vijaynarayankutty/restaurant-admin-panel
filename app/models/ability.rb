class Ability
  include CanCan::Ability

  def initialize(admin)
    
    admin ||= Admin.new # guest user (not logged in)
    # if admin.super_admin?

    if admin.super_admin?
		can :access, :rails_admin   # grant access to rails_admin
		can :dashboard 

      	can :manage, Restaurant
      	can :manage, Admin
      	can :manage, Dish
        # can :manage, TableManagement
      	can :manage, DishCategory
      	can :manage, RestaurantCategory
        # can :manage, OrderDetail
      	can :manage, Order

        # can :manage, DetailOrder

        can :manage, RestaurantAdmin
        cannot [ :export, :history, :destroy, :new ], Order
        # cannot [ :export ], Restaurant
        can [:show, :edit, :destroy, :history], Order
      	# can :manage, User 
        can :index, User
        can [ :export, :history, :destroy], User
        can [:show, :edit, :destroy, :history], User
 
end
  end
end

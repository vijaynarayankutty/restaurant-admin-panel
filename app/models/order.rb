class Order < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :user
  has_many   :order_details
  has_many   :dishes, through: :order_details
# 
  scope :open_orders, -> { where(active: true) }
  scope :closed_orders, -> { where(active: false) }

  # validates :user_id, :restaurant, presence: true
  # validates :table_no, numericality: { greater_than_or_equal_to: 0}, presence: true
  # validates :amount, numericality: {greater_than_or_equal_to: 0}, presence: true

  # def get_order
  # 	self.order_details
  # end

  def get_details
  	self.order_details
  end



  def username
  	"<a href='user/#{self.user.id}/'>#{self.user.first_name}</a>".html_safe
  end

	rails_admin do
		list do
			field :username
			field :amount
			# field :tax
			# field :tip
			field :table_no
			field :restaurant
	    end
	end
end

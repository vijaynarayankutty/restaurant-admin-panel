class Restaurant < ActiveRecord::Base
  belongs_to :restaurant_admin
  belongs_to :restaurant_category

  has_many  :table_management
  accepts_nested_attributes_for :table_management
	has_many :dishes
	has_many :orders
  belongs_to :user

  validate :validate_available_from, :validate_available_from_equality
  validate :validate_from_time
  

	accepts_nested_attributes_for :dishes, allow_destroy: true
	# has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"


#Content uploading to S3 Bucket
  has_attached_file  :image,
                     :styles =>  {:medium => "300x300", :thumb => "100x100"},
                     :storage => :s3,
                     :s3_credentials => S3_CONFIG,
                     :bucket => APP_CONFIG["bucket"],
                     :default_url => ":s3_domain_url",
                     :preserve_files => true
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

#Adding logo to S3 Bucket
  has_attached_file  :restaurant_logo,
                     :styles =>  {:medium => "300x300", :thumb => "100x100"},
                     :storage => :s3,
                     :s3_credentials => S3_CONFIG,
                     :bucket => APP_CONFIG["bucket"],
                     :default_url => ":s3_domain_url",
                     :preserve_files => true
  validates_attachment_content_type :restaurant_logo, :content_type => /\Aimage\/.*\Z/

# Validations
 
         # validates :restaurant_name, presence: true, length: {maximum: 20}
         # validates :restaurant_category, presence: true
         # validates :restaurant_admin, presence: true
         # validates :image, presence: true
         # validates :address1, presence: true, length: {maximum: 50}
         # validates :address2, presence: true, length: {maximum: 50}
         # validates :description, presence: true, length: {maximum: 1000}
         # validates :country, presence: true
         # validates :countrycode, presence: true, :numericality => true
         # validates :city, presence: true
         # validates :location, presence: true
         # validates :latitude, presence: true, :numericality => true
         # validates :longitude, presence: true, :numericality => true
         # validates :city, presence: true, length: {maximum: 20}
         # validates :no_of_tables, presence: true, numericality: { greater_than_or_equal_to: 0 }
         # validates :from_time, presence: true
         # validates :to_time, presence: true
         # validates :available_from, presence: true
         # validates :available_to, presence: true
         # validates :dining_cost_rating, presence: true
         # validates :tax, presence: true, numericality: { greater_than_or_equal_to: 0 }
         # validates :available_from, presence: true
         # validates :available_to, presence: true
         # validates :from_time, presence: true
         # validates :to_time, presence: true


#Custom validation goes here------------------------
           

   def validate_available_from
     if available_from.present? && available_to < available_from
       errors.add(:available_to, "can't be  past")
     end
   end

   def validate_available_from_equality
     if available_from.present? && available_to == available_from
      errors.add(:available_to, "can't be equal")
    end
  end

    def validate_from_time
      if from_time.present? && from_time > to_time
        errors.add(:to_time, "can't be past")
      end
    end

    
   # def validate_from_time
   #  if from_time.strftime.present? && from_time.strftime < to_time.strftime
   #    errors.add(:to_time, "can't be less than")
   #  end

    # def validate_from_time_equality
    #   if from_time.present? && from_time == to_time
    #     errors.add(:to_time, "can't be equal")
    #   end
    # end


 

  def restaurant_link
    "<a href='restaurant/#{self.id}/'>#{self.restaurant_name}</a>".html_safe
  end

  def from_time
    if self.available_from.present?
        temp_time = self.available_from.to_s.split(':')     
        Time.parse("#{temp_time[0]}:#{temp_time[1]}").strftime("%l %P")
    end
  end

  def to_time
    if self.available_to.present?
       temp_time = self.available_to.to_s.split(':')
       Time.parse("#{temp_time[0]}:#{temp_time[1]}").strftime("%l %P")
   	end
  end

  def name
		self.restaurant_name
	end
 
 # def dining_cost_rating_enum
 #   # [['Active', 1],['Pending',0],['Banned',2]]
 #   [['Active'],['Pending'],['Banned']]
 # end
  
  def get_orders
    self.orders.open_orders
  end

  def get_closed_orders
    self.orders.closed_orders
  end

#For listing dishes inside the restaurant 
  def get_dishes
    self.dishes  
  end



  

 	rails_admin do
      list do
          field :restaurant_link do
              label "RestaurantName"
          end
          field :restaurant_category
          field :no_of_tables
          field :from_time do
            label "Opening Time"
          end

          field :to_time do 
            label "Closing Time"
          end
          field :country 
          field :tax do 
            label "Restaurant Tax"
          end
      end

      edit do
         field :restaurant_name do 
            label "Restaurant Name"
          end
         field :image
         field :restaurant_logo do 
          label "Restaurant Logo"
         end
         field :short_Address
         field :long_Address
         field :description
         field :restaurant_admin do
          label "Restaurant Admin"
        end
         field :no_of_tables do
          label "No of Tables"
        end

         field :dining_cost_rating, :enum do 
            label "Dining Cost Rating"
            enum do
               %w(1 2 3 4)
            end
         end
         
         field :tax do
          label "Restaurant Tax"
        end
         field :restaurant_category do 
           label "Restaurant Category"
         end
         field :dishes 
         field :table_management do
          label "Manage Table"
        end

         field :country 
         field :countrycode do 
           label "Country Code"
         end
         field :city
         # field :location
         field :latitude
         field :longitude
         field :from_time do 
          label "Opening Time"
        end
         field :to_time do 
           label "Closing Time"
         end
         field :available_from do
          label "Active From"
         end
         field :available_to do
          label "Active To"
         end
         field :active
      end
  end
end

class Dish < ActiveRecord::Base
	belongs_to :dish_category
	belongs_to :restaurant
	has_many :order_details
	has_many :orders, through: :order_details

	before_create :process_options
		def process_options
			self.parameter1_options = self.parameter1_options.gsub("-", "%")  if self.parameter1_options.present?
		end

	# validate :validate_availablefrom, :validate_availablefrom_equality
	
	def restaurant_name
		if self.restaurant.present?
		   "<a href='restaurant/#{self.restaurant.id}/'>#{self.restaurant.restaurant_name}</a>".html_safe
		else
			""
		end
	end
      
  #Adding image to dish.rb

	has_attached_file  :image, 
	                   :styles =>  {:medium => "300x300", :thumb => "100x100"},
	                   :storage => :s3,
	                   :s3_credentials => S3_CONFIG,
	                   :bucket => APP_CONFIG["bucket"],
	                   :default_url => ":s3_domain_url",
	                   :preserve_files => true
	                   
	 validates_attachment_content_type :image,
	 								    :content_type => /^image\/(png|gif|jpeg)/,
                                        :message => 'only (png/gif/jpeg) images'
	# validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
   
     before_save { self.name = name.downcase }
  #    validates :name, presence: true, length: { maximum: 20 },uniqueness: {case_sensitive: false}
  #    validates :description, presence: true, length: {maximum: 1000}
	 # validates :price, presence: true, numericality: true
	 # # validates :availablefrom, presence: true
	 # # validates :availableto, presence: true
	 # validates :dish_category, presence: true
	 # validates :image, presence: true

	 # def validate_availablefrom
  #     if availablefrom.present? && availablefrom > availableto
  #       errors.add(:availableto, "can't be past")
  #     end
  #   end

  #   def validate_availablefrom_equality
  #   	if availablefrom.present? && availablefrom == availableto
  #   		errors.add(:availableto, "can't be equal")
  #   	end
  #   end

 #Listing Time on View
     def from_time
    if self.availablefrom.present?
        temp_time = self.availablefrom.to_s.split(':')     
        Time.parse("#{temp_time[0]}:#{temp_time[1]}").strftime("%l %P")
    end
  end

  def to_time
    if self.availableto.present?
       temp_time = self.availableto.to_s.split(':')
       Time.parse("#{temp_time[0]}:#{temp_time[1]}").strftime("%l %P")
   	end
  end


	rails_admin do
		list do
			field :name

			field :description
			field :price
			#field :availabletime
			#field :availablefrom
			#field :availableto
			field :dish_category
			field :restaurant_name
			# field :availablefrom
		 #    field :availableto
		    field :from_time
		    field :to_time

			# field :restaurant_name
		end

		edit do
			field :name
			field :description
			field :image
			field :price
			field :dish_category
			#field :restaurant
			#field :restaurant_name
			#field :availabletime
			field :active
			field :availablefrom
		    field :availableto
		     # field :from_time
		     # field :to_time
		  #    field    :parameter_1 do
		  #    	label "Dish-Parameter-1"
		  #     end
		  #    field    :parameter1_options do
		  #    	label "Param-1-Options"
		  #     end 
    #          field    :parameter_2
    #          field    :parameter2_options
			 # field    :parameter_3
			 # field    :parameter3_options 
	   #       field    :parameter_4  
	   #       field    :parameter4_options          
	   #       field    :parameter_5                    
    #          field    :parameter5_options          
    #          field    :parameter_1_price_1                       
    #          field    :parameter_1_price_2                       
    #          field    :parameter_1_price_3                       
    #          field    :parameter_2_price_1                       
    #          field    :parameter_2_price_2                       
    #          field    :parameter_2_price_3                       
    #          field    :parameter_3_price_1                       
    #          field    :parameter_3_price_2                       
    #          field    :parameter_3_price_3                       
    #          field    :parameter_4_price_1                       
    #          field    :parameter_4_price_2                       
    #          field    :parameter_4_price_3                       
    #          field    :parameter_5_price_1                       
    #          field    :parameter_5_price_2                       
    #          field    :parameter_5_price_3                      

		end

		 visible false

		

		
	end
end
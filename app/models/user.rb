class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :auth_token, uniqueness: true

  # validates :first_name, presence: true, length: {maximum: 20}
  # validates :last_name, presence: true, length: {maximum: 20}
  # validates :email, presence:true
  
  # validates :country, presence: true, length: {maximum: 20}
  # validates :countrycode, presence: true, length: {maximum: 5}
  # validates :restaurants, presence: true
  # validates :address_one, presence: true, length: {maximum: 50}
  # validates :address_two, presence: true, length: {maximum: 30}
  # validates :mobile,:presence => true,
  #                :numericality => true,
  #                :length => { :minimum => 10, :maximum => 15 }

  # validates :password_confirmation, presence: true, :length => { :minimum => 8, :maximum => 128 }

  before_create :generate_authentication_token!
  has_many :orders
  has_many :restaurants #, :conditions => { :role => "admin" }

  def generate_authentication_token!
    begin
      self.auth_token = Devise.friendly_token
    end while self.class.exists?(auth_token: auth_token)
    # self.role ||= 'user'
  end

  def name
    first_name    
  end



  rails_admin do
    label "Customers"

    list do


      field :first_name
      field :email
      
      field :country
      field :mobile
    end

    edit do
      field  :first_name
      field  :last_name
      field  :email
      field  :password
      field  :password_confirmation
      # field :role, :enum do
      #   enum do
      #     [['User', 'user'], ['', '']]
      #   end
      # end
      # field  :restaurants
      field  :address_one
      field  :address_two
      field  :country
      field  :countrycode
      #field  :city
     # field  :zipcode
      field  :mobile
      # field  :CreditCard
      # field  :CVV
      # field  :refferal_total
      # field  :provider_name
      
     
     end
  end
end

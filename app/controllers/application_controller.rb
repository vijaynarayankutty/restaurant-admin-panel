class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session
  skip_before_filter  :verify_authenticity_token
  include Authenticable


  def after_sign_in_path_for(admin)
    return rails_admin.dashboard_path
  end


  # Overwriting the sign_out redirect path method
  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  # rescue_from CanCan::AccessDenied do |exception|
  #   redirect_to static_pages_page_404_path, :alert => "hi"
  # end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to "/static_pages/page_404"
    ## to avoid deprecation warnings with Rails 3.2.x (and incidentally using Ruby 1.9.3 hash syntax)
    ## this render call should be:
    # render file: "#{Rails.root}/public/403", formats: [:html], status: 403, layout: false
  end

end


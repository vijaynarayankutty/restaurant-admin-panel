class DishesController < ApplicationController

  before_action :set_dish, only: [:show, :edit, :update, :destroy]
  # before_action :authenticate_with_token!
  respond_to :html
  respond_to :json

  def index
    @dishes = Dish.all
    respond_with(@dishes)
  end

  def show
    respond_with(@dish)
  end

  def new
    @dish = Dish.new
    respond_with(@dish)
  end

  def edit
  end

  def create
    dish_params_temp = dish_params
    dish_params_temp[:parameter1_options] = ""
    if dish_params_temp[:dish_temp1].present?
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + dish_params_temp[:dish_temp1].to_s + "%"
    else
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + "nill%"
    end
    dish_params_temp.delete("dish_temp1")

    p dish_params_temp

    if dish_params_temp[:dish_temp2].present?
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + dish_params_temp[:dish_temp2].to_s + "%"
    else
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + "nill%"
    end
    dish_params_temp.delete(:dish_temp2)

    if dish_params_temp[:dish_temp3].present?
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + dish_params_temp[:dish_temp3].to_s + "%"
    else
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + "nill%"
    end
    dish_params_temp.delete(:dish_temp3)

    if dish_params_temp[:dish_temp4].present?
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + dish_params_temp[:dish_temp4].to_s + "%"
    else
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + "nill%"
    end
    dish_params_temp.delete(:dish_temp4)

    if dish_params_temp[:dish_temp5].present?
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + dish_params_temp[:dish_temp5].to_s + "%"
    else
      dish_params_temp[:parameter1_options] =  dish_params_temp[:parameter1_options].to_s + "nill%"
    end
    dish_params_temp.delete(:dish_temp5)


    #---------------------------------------------------------------------------------------

     dish_params_temp[:parameter2_options] = ""
    if dish_params_temp[:parameter_2_temp_1].present?
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + dish_params_temp[:parameter_2_temp_1].to_s + "%"
    else
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_2_temp_1)

    if dish_params_temp[:parameter_2_temp_2].present?
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + dish_params_temp[:parameter_2_temp_2].to_s + "%"
    else
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_2_temp_2)

    if dish_params_temp[:parameter_2_temp_3].present?
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + dish_params_temp[:parameter_2_temp_3].to_s + "%"
    else
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_2_temp_3)

    if dish_params_temp[:parameter_2_temp_4].present?
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + dish_params_temp[:parameter_2_temp_4].to_s + "%"
    else
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_2_temp_4)


    if dish_params_temp[:parameter_2_temp_5].present?
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + dish_params_temp[:parameter_2_temp_5].to_s + "%"
    else
      dish_params_temp[:parameter2_options] =  dish_params_temp[:parameter2_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_2_temp_5)


    #---------------------------------------------------------------------------------------------------


    dish_params_temp[:parameter3_options] = ""
    if dish_params_temp[:parameter_3_temp_1].present?
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + dish_params_temp[:parameter_3_temp_1].to_s + "%"
    else
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_3_temp_1)

    if dish_params_temp[:parameter_3_temp_2].present?
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + dish_params_temp[:parameter_3_temp_2].to_s + "%"
    else
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_3_temp_2)

    if dish_params_temp[:parameter_3_temp_3].present?
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + dish_params_temp[:parameter_3_temp_3].to_s + "%"
    else
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_3_temp_3)

    if dish_params_temp[:parameter_3_temp_4].present?
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + dish_params_temp[:parameter_3_temp_4].to_s + "%"
    else
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_3_temp_4)


    if dish_params_temp[:parameter_3_temp_5].present?
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + dish_params_temp[:parameter_3_temp_5].to_s + "%"
    else
      dish_params_temp[:parameter3_options] =  dish_params_temp[:parameter3_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_3_temp_5)

    #-------------------------------------------------------------------------------------------------------------------------------


    dish_params_temp[:parameter4_options] = ""
    if dish_params_temp[:parameter_4_temp_1].present?
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + dish_params_temp[:parameter_4_temp_1].to_s + "%"
    else
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_4_temp_1)

    if dish_params_temp[:parameter_4_temp_2].present?
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + dish_params_temp[:parameter_4_temp_2].to_s + "%"
    else
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_4_temp_2)

    if dish_params_temp[:parameter_4_temp_3].present?
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + dish_params_temp[:parameter_4_temp_3].to_s + "%"
    else
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_4_temp_3)

    if dish_params_temp[:parameter_4_temp_4].present?
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + dish_params_temp[:parameter_4_temp_4].to_s + "%"
    else
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_4_temp_4)


    if dish_params_temp[:parameter_4_temp_5].present?
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + dish_params_temp[:parameter_4_temp_5].to_s + "%"
    else
      dish_params_temp[:parameter4_options] =  dish_params_temp[:parameter4_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_4_temp_5)

#-------------------------------------------------------------------------------------------------

  dish_params_temp[:parameter5_options] = ""
    if dish_params_temp[:parameter_5_temp_1].present?
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + dish_params_temp[:parameter_5_temp_1].to_s + "%"
    else
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_5_temp_1)

    if dish_params_temp[:parameter_5_temp_2].present?
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + dish_params_temp[:parameter_5_temp_2].to_s + "%"
    else
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_5_temp_2)

    if dish_params_temp[:parameter_5_temp_3].present?
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + dish_params_temp[:parameter_5_temp_3].to_s + "%"
    else
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_5_temp_3)

    if dish_params_temp[:parameter_5_temp_4].present?
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + dish_params_temp[:parameter_5_temp_4].to_s + "%"
    else
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_5_temp_4)


    if dish_params_temp[:parameter_5_temp_5].present?
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + dish_params_temp[:parameter_5_temp_5].to_s + "%"
    else
      dish_params_temp[:parameter5_options] =  dish_params_temp[:parameter5_options].to_s + "nill%"
    end
    dish_params_temp.delete(:parameter_5_temp_5)



    
    @dish = Dish.new(dish_params_temp)
    p dish_params_temp
    @dish.save
    redirect_to "/admin/restaurant/#{@dish.restaurant_id}/add_dish_path"
    # respond_with(@dish)
  end

  def update
    @dish.update(dish_params_temp)
    respond_with(@dish)
  end

  def destroy
    @dish.destroy
    respond_with(@dish)
  end

  private
    def set_dish
      @dish = Dish.find(params[:id])
    end

    def dish_params
      params.permit(:name, :restaurant_id, :description, :dish_category_id , :price, :availablefrom, :availableto, 
                  :dish_temp1, :dish_temp2, :dish_temp3, :dish_temp4, :dish_temp5, :parameter_2_temp_1, :parameter_2_temp_2, 
                  :parameter_2_temp_3, :parameter_2_temp_4, :parameter_2_temp_5, :parameter_3, :parameter_3_temp_1, 
                  :parameter_3_temp_2, :parameter_3_temp_3, :parameter_3_temp_4, :parameter_3_temp_5, :parameter_4, 
                  :parameter_4_temp_1, :parameter_4_temp_2, :parameter_4_temp_3, :parameter_4_temp_4, :parameter_4_temp_5, 
                  :parameter_5, :parameter_5_temp_1, :parameter_5_temp_2, :parameter_5_temp_3, :parameter_5_temp_4, 
                  :parameter_5_temp_5, :parameter_1_price_1, :parameter_1_price_2, :parameter_1_price_3, :parameter_1_price_4, 
                  :parameter_1_price_5, :parameter_2_price_1, :parameter_2_price_2, :parameter_2_price_3, :parameter_2_price_4, 
                  :parameter_2_price_5, :parameter_3_price_1, :parameter_3_price_2, :parameter_3_price_3, :parameter_3_price_4, 
                  :parameter_3_price_5, :parameter_4_price_1, :parameter_4_price_2, :parameter_4_price_3, :parameter_4_price_4, 
                  :parameter_4_price_5, :parameter_5_price_1, :parameter_5_price_2, :parameter_5_price_3, :parameter_5_price_4, 
                  :parameter_5_price_5, :parameter1_options, :parameter2_options, :parameter3_options,
                  :parameter4_options, :parameter5_options,:dish_property_chilly, :dish_property_vegan,:dish_property_glutenFree,
                  :dish_property_vegetarian,:dish_property_nutFree, :image, :availablefrom, :availableto, :active)
    end
end

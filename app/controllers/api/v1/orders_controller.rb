class Api::V1::OrdersController < ApplicationController
  before_action :set_order, only: [:show, :edit, :update, :destroy]
  #before_action :authenticate_with_token!
  respond_to :html
  respond_to :json
  
  def index
    # @orders = current_user.orders
    # respond_with(@orders)
    respond_with Order.all
  end

  #Need to add current_user

  def show
    # respond_with(@order)
    respond_with Order.find(params[:id])
  end

  def new
    @order = Order.new
    respond_with(@order)
  end

  def edit
  end

  # def create
  #   p params.to_yaml
  #   @order = Order.new(order_params)
  #   @order.save
  #   respond_with(@order)
  # end

  def create
    order = current_user.orders.build(order_params)
    if order.save
      render json: order, status: 201 location: [:api, order]
    else
      render json: { errors: order.errors }, status: 422
    end
  end

  # def update
  #   @order.update(order_params)
  #   respond_with(@order)
  # end 

  def update
    order = current_user.orders.find(params[:id])
    if order.update(order_params)
      render json: order, status: 200 location: [:api, order]
    else
      render json: { errors: order.errors }, status: 422
  end

  # def destroy
  #   @order.destroy
  #   respond_with(@order)
  # end

  def destroy
    order = current_user.orders.find(params[:id])
    order.destroy
    head 204
  end

  private
    def set_order
      @order = Order.find(params[:id])
    end

    def order_params
      params.require(:order).permit(:user_id, :user_name, :tax, :tip, :transaction_id, :table_no, :date_of_order, :active, :time_to_dispatch_order, :restaurant_id)
    end
end

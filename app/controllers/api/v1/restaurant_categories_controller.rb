class Api::V1::RestaurantCategoriesController < ApplicationController

  before_action :set_restaurant_category, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @restaurant_categories = RestaurantCategory.all
    respond_with(@restaurant_categories)
  end

  def show
    respond_with(@restaurant_category)
  end

  def new
    @restaurant_category = RestaurantCategory.new
    respond_with(@restaurant_category)
  end

  def edit
  end

 
   def create
    @restaurant_category = RestaurantCategory.new(restaurant_category_params)
    @restaurant_category.save
    respond_with(@restaurant_category)
  end

  def update
    @restaurant_category.update(restaurant_category_params)
    respond_with(@restaurant_category)
  end

  def destroy
    @restaurant_category.destroy
    respond_with(@restaurant_category)
  end

  private
    def set_restaurant_category
      @restaurant_category = RestaurantCategory.find(params[:id])
    end

    def restaurant_category_params
      params.require(:restaurant_category).permit(:name)
    end
end

class Api::V1::DishCategoriesController < ApplicationController

 
  before_action :set_dish_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_with_token!
  respond_to :html

  def index
    @dish_categories = DishCategory.all
    respond_with(@dish_categories)
  end

  def show
    respond_with(@dish_category)
  end

  def new
    @dish_category = DishCategory.new
    respond_with(@dish_category)
  end

  def edit
  end

  def create
    @dish_category = DishCategory.new(dish_category_params)
    @dish_category.save
    respond_with(@dish_category)
  end

  def update
    @dish_category.update(dish_category_params)
    respond_with(@dish_category)
  end

  def destroy
    @dish_category.destroy
    respond_with(@dish_category)
  end

  private
    def set_dish_category
      @dish_category = DishCategory.find(params[:id])
    end

    def dish_category_params
      params.require(:dish_category).permit(:name)
    end
end

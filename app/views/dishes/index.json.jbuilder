json.array!(@dishes) do |dish|
  json.extract! dish, :id, :name, :description, :price, :availabletime, :active
  json.url dish_url(dish, format: :json)
end

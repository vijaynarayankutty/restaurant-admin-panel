json.array!(@restaurants) do |restaurant|
  json.extract! restaurant, :id, :restaurant_name, :description, :available_from, :available_to, :no_of_tables, :email, :address1, :address2, :country, :countrycode, :location, :city, :phonenumber, :latitude, :longitude, :admin_id, :restaurant_category_id, :active
  json.url restaurant_url(restaurant, format: :json)
end

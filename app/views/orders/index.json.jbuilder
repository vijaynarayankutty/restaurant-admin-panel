json.array!(@orders) do |order|
  json.extract! order, :id, :amount, :tax, :tip, :transaction_id, :table_no, :time_to_dispatch_order, :dine_in, :restaurant_id, :user_id
  json.url order_url(order, format: :json)
end
